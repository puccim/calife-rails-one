class CreateCadavers < ActiveRecord::Migration[5.1]
  def change
    create_table :cadavers do |t|
      t.string :name
      t.string :surname
      t.date :birth
      t.date :death

      t.timestamps
    end
  end
end
