require 'test_helper'

class CadaversControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cadaver = cadavers(:one)
  end

  test "should get index" do
    get cadavers_url
    assert_response :success
  end

  test "should get new" do
    get new_cadaver_url
    assert_response :success
  end

  test "should create cadaver" do
    assert_difference('Cadaver.count') do
      post cadavers_url, params: { cadaver: { birth: @cadaver.birth, death: @cadaver.death, name: @cadaver.name, surname: @cadaver.surname } }
    end

    assert_redirected_to cadaver_url(Cadaver.last)
  end

  test "should show cadaver" do
    get cadaver_url(@cadaver)
    assert_response :success
  end

  test "should get edit" do
    get edit_cadaver_url(@cadaver)
    assert_response :success
  end

  test "should update cadaver" do
    patch cadaver_url(@cadaver), params: { cadaver: { birth: @cadaver.birth, death: @cadaver.death, name: @cadaver.name, surname: @cadaver.surname } }
    assert_redirected_to cadaver_url(@cadaver)
  end

  test "should destroy cadaver" do
    assert_difference('Cadaver.count', -1) do
      delete cadaver_url(@cadaver)
    end

    assert_redirected_to cadavers_url
  end
end
