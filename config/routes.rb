Rails.application.routes.draw do

  # get 'static_pages/login'
  # get 'static_pages/home'
  # get 'static_pages/help'
  # get 'static_pages/about'

  get  '/help',    to: 'static_pages#help'
  get  '/about',    to: 'static_pages#about'
  get  '/home',    to: 'static_pages#home'
  get  '/login',  to: 'static_pages#login'
  get  '/contact',  to: 'static_pages#contact'
  get  '/signup',  to: 'users#new'

  resources :microposts
  resources :cadavers
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # root 'application#hello'

  root 'static_pages#home'

end
