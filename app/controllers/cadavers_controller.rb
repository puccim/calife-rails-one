class CadaversController < ApplicationController
  before_action :set_cadaver, only: [:show, :edit, :update, :destroy]

  # GET /cadavers
  # GET /cadavers.json
  def index
    @cadavers = Cadaver.all
  end

  # GET /cadavers/1
  # GET /cadavers/1.json
  def show
  end

  # GET /cadavers/new
  def new
    @cadaver = Cadaver.new
  end

  # GET /cadavers/1/edit
  def edit
  end

  # POST /cadavers
  # POST /cadavers.json
  def create
    @cadaver = Cadaver.new(cadaver_params)

    respond_to do |format|
      if @cadaver.save
        format.html { redirect_to @cadaver, notice: 'Cadaver was successfully created.' }
        format.json { render :show, status: :created, location: @cadaver }
      else
        format.html { render :new }
        format.json { render json: @cadaver.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cadavers/1
  # PATCH/PUT /cadavers/1.json
  def update
    respond_to do |format|
      if @cadaver.update(cadaver_params)
        format.html { redirect_to @cadaver, notice: 'Cadaver was successfully updated.' }
        format.json { render :show, status: :ok, location: @cadaver }
      else
        format.html { render :edit }
        format.json { render json: @cadaver.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cadavers/1
  # DELETE /cadavers/1.json
  def destroy
    @cadaver.destroy
    respond_to do |format|
      format.html { redirect_to cadavers_url, notice: 'Cadaver was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cadaver
      @cadaver = Cadaver.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cadaver_params
      params.require(:cadaver).permit(:name, :surname, :birth, :death)
    end
end
