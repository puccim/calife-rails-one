class User < ApplicationRecord
  has_many :microposts

  validates :name , :presence => true

  validates :surname, :presence => true

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX }

  before_save {self.email.downcase!}

  has_secure_password

  validates :password, length: { minimum: 6 }

end
