json.extract! cadaver, :id, :name, :surname, :birth, :death, :created_at, :updated_at
json.url cadaver_url(cadaver, format: :json)
