json.extract! user, :id, :name, :surname, :created_at, :updated_at, :email
json.url user_url(user, format: :json)
